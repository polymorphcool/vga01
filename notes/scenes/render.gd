extends Sprite

var mat = null
var a = 0

export(float, 0, 1 ) var red_shift = 0
export(float, 0, 1 ) var green_shift = 0
export(float, 0, 1 ) var blue_shift = 0

func _ready():
	material.set_shader_param( "y_limit", 0.7 )
	pass

func _process(delta):
	
	material.set_shader_param( "decal_red_x", ( randf() - 0.5 ) * red_shift )
	material.set_shader_param( "decal_green_x", ( randf() - 0.5 ) * green_shift )
	material.set_shader_param( "decal_blue_y", ( randf() - 0.5 ) * blue_shift )
	a += delta
	pass
