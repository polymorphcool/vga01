extends MeshInstance

var wait = 0
var mat
var bear
var bearmat

func _ready():
	mat = get_surface_material( 0 )
	bear = get_node( "BearRig/Bear" )
	bearmat = bear.get_surface_material( 0 )
	for i in range(0,10):
		var b2 = bear.duplicate()
		self.add_child( b2 )
		b2.translate( Vector3( 2.1 * i,0,0 ) )
	print( bear )
	pass

func _process(delta):
	if wait > 5:
		mat.albedo_color = Color( randf(),randf(),randf(),1 )
		bearmat.albedo_color = Color( randf(),randf(),randf(),1 )
		wait = 0
	wait += delta
	mat.uv1_offset += Vector3( -0.001, 0.001, 0 )