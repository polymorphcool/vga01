![banner](https://polymorph.cool/wp-content/uploads/2018/10/Selection_999053.png)

*Video Game for Art*

sources of the examples seen during the session of 27th & 28th October

[page on polymorph.cool](https://polymorph.cool/2018/10/25/video-game-for-art/)

## inside


all projects created for & during the 2 days of workshop

### What has been prepared

![](https://polymorph.cool/wp-content/uploads/2018/10/Godot-Engine-3D-basis-01_primitives.tscn_803-768x432.png)

**01_3d_basis**, *scene/01_primitives.tscn* primitives and parenting

![](https://polymorph.cool/wp-content/uploads/2018/10/Godot-Engine-3D-basis-02_material.tscn_804-768x432.png)

**01_3d_basis**, *scene/02_material.tscn*: materials & textures

![](https://polymorph.cool/wp-content/uploads/2018/10/Godot-Engine-3D-basis-03_environment.tscn_805-768x432.png)

**01_3d_basis**, *scene/03_environment.tscn*: environment

![](https://polymorph.cool/wp-content/uploads/2018/10/Godot-Engine-3D-basis-04_skeleton.tscn_806-768x432.png)

**01_3d_basis**, *scene/04_skeleton.tscn*: collada, importing mesh, skeleton & animation

![](https://polymorph.cool/wp-content/uploads/2018/10/Workspace-1_807-768x432.png)

**02_3d_dynamics**: complex physic, multiple shapes in one collider + a lot of spheres

![](https://polymorph.cool/wp-content/uploads/2018/10/Workspace-1_801-768x432.png)

**03_scripting** & **04_ui animation**: in script + 2D UI over the 3D view, with animation also

### created during the session

![](https://polymorph.cool/wp-content/uploads/2018/10/Workspace-1_800-768x432.png)

**notes**, *scenes/viewport.tscn*: an example on how to render on texture and use a 2D shader to shift RGB channels

![](https://polymorph.cool/wp-content/uploads/2018/10/Workspace-1_799-768x432.png)

**notes**, *scenes/script.tscn*: animation and mouse interaction via script + node duplication

![](https://polymorph.cool/wp-content/uploads/2018/10/Workspace-1_799-768x432.png)

**notes**, *scenes/script.tscn*: animation and mouse interaction via script + node duplication

![](https://polymorph.cool/wp-content/uploads/2018/10/Godot-Engine-Yo-deux.tscn_797-768x432.png)

**notes**, *scenes/deux.tscn*: importation of scenes and collada

![](https://polymorph.cool/wp-content/uploads/2018/10/Godot-Engine-Yo-un.tscn_796-768x432.png)

**notes**, *scenes/un.tscn*: 3d basics: primitives, parenting, environment and materials


## resources

* [official website](https://godotengine.org/)
* [official repository](https://github.com/godotengine/godot)
* [documentation](http://docs.godotengine.org/en/3.0/)
* [official demos](https://github.com/godotengine/godot-demo-projects)
* [demos by gokudomatic](https://github.com/gokudomatic/godot/tree/master/demos/)